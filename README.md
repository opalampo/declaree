- docker-compose up -d
- In php container
  - composer install 
  - php bin/console doctrine:database:create
  - php bin/console doctrine:migrations:migrate
  - npm install yarn
  - yarn encore (to build the assets)
  

The frontend has a navigation bar with two links, so no need for explanations there.
The guide mentioned making two specific currencies refreshable (IND and EUR). However, I could not find IND anywhere in the list
of retrieved currencies. Also, unless I missunderstood something I could not see the point in makin only these two refresh instead
of all the rates, so I just made a view which has a table with all the retrieved rates. There is a sync button on the top right of
the table which pulls the current data from the API and refreshes the view.

The second view will be empty the first time it is visited after the project is built up since the DB is empty. As soon as you click the
sync button on the table it will fetch the data from the endpoint, store it in the DB and reload the view.
There is a "Add rate" button on top, which brings up a form to enter the currency name and rate compared to USD. Once submitted, the
view will refresh and show the new rate at the bottom of the table. However, clicking the refresh button will truncate the table and pull the
data from the API endpoint again, so any manually added rates will be lost.

Finally, each row has a delete button, without confirmation. So as soon as it is clicked the record is deleted from the DB and the view is refreshed.
I should mention, I have been a bit stretched for time, so I opted not to continue with implementing the edit functionality as well.