<?php

namespace App\Controller;

use App\Entity\Rate;
use App\Repository\RateRepository;
use App\Service\RateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RateController extends AbstractController
{
    /** @var RateService */
    private $rateService;

    /** @var RateRepository */
    private $rateRepository;

    public function __construct(
        RateService $rateService,
        RateRepository $rateRepository
    )
    {
        $this->rateService = $rateService;
        $this->rateRepository = $rateRepository;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $response = $this->rateService->getRatesWithUsdBase();

        if ($response->getStatusCode() != 200) {
            throw new \Exception('The request to the exchange rates API failed. Please refresh to try again');
        }

        return $this->render('rate/index.html.twig', [
            'baseRate' => json_decode($response->getBody())->base,
            'rates' => (array) json_decode($response->getBody())->rates,
            'date' => json_decode($response->getBody())->date,
            'title' => 'Exchanger | Live rates'
        ]);
    }

    /**
     * @Route("/rates", name="rates", methods={"GET"})
     */
    public function rates()
    {
        $rates = $this->rateRepository->findAll();
        $response = $this->rateService->getRatesWithUsdBase();

        if ($response->getStatusCode() != 200) {
            throw new \Exception('The request to the exchange rates API failed. Please refresh to try again');
        }

        $deleteForm = $this->createFormBuilder()
            ->setAction($this->generateUrl('delete-rate', ['rateId' => 0]))
            ->setMethod('DELETE')
            ->getForm();

        $postForm = $this->createFormBuilder()
            ->setAction($this->generateUrl('post-rate'))
            ->setMethod('POST')
            ->getForm();


        return $this->render('rate/stored.html.twig', [
            'baseRate' => json_decode($response->getBody())->base,
            'rates' => $rates,
            'deleteForm' => $deleteForm->createView(),
            'postForm' => $postForm->createView(),
            'title' => 'Exchanger | Stored rates'
        ]);
    }

    /**
     * @Route("/refresh-db-rates", name="refresh-db-rates")
     */
    public function refreshAllStoredRates()
    {
        $truncateRateTable = $this->rateRepository->truncateRateTable();

        if ($truncateRateTable) {
            $response = $this->rateService->getRatesWithUsdBase();
            $rates = (array) json_decode($response->getBody())->rates;

            $this->rateRepository->persistRatesArray($rates);
        } else {
            throw new \Exception('The rates table could not be truncated.');
        }

        return $this->redirect($this->generateUrl('rates'));
    }

    /**
     * @Route("/rates/{rateId}", name="delete-rate", methods={"DELETE"})
     */
    public function deleteRate(int $rateId)
    {
        try {
            $this->rateRepository->deleteRate($rateId);

            return $this->redirect($this->generateUrl('rates'));
        } catch (\Exception $exception) {
            throw new \Exception('Something went wrong with the deletion.');
        }
    }

    /**
     * @Route("/rates", name="post-rate", methods={"POST"})
     */
    public function createRate(Request $request)
    {
        try {
            $rateInstance = new Rate();
            $rateInstance->setCurrency($request->get('currency'));
            $rateInstance->setRate($request->get('rate'));

            $this->rateRepository->persistRateInstance($rateInstance);

            return $this->redirect($this->generateUrl('rates'));
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }
}
