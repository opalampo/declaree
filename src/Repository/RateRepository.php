<?php

namespace App\Repository;

use App\Entity\Rate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Rate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rate[]    findAll()
 * @method Rate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Rate::class);
    }

    public function truncateRateTable() : bool
    {
        try {
            $entityManager = $this->getEntityManager();

            $connection = $entityManager->getConnection();
            $platform = $connection->getDatabasePlatform();
            $connection->executeUpdate($platform->getTruncateTableSQL('rate', true));
        } catch (\Exception $exception) {
            return false;
        }

        return true;
    }

    public function persistRatesArray(array $rates)
    {
        $entityManager = $this->getEntityManager();

        foreach ($rates as $currency => $rate) {
            $rateInstance = new Rate();
            $rateInstance->setCurrency($currency);
            $rateInstance->setRate($rate);

            $entityManager->persist($rateInstance);
        }

        $entityManager->flush();
    }

    public function persistRateInstance(Rate $rate)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($rate);
        $entityManager->flush();
    }

    public function deleteRate(int $rateId)
    {
        $entityManager = $this->getEntityManager();
        $rate = $this->find($rateId);

        $entityManager->remove($rate);
        $entityManager->flush();
    }
}
