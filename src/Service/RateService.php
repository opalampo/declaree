<?php


namespace App\Service;


class RateService
{
    private $endpoints = [
        'allRatesBaseUsd' => 'https://api.exchangeratesapi.io/latest?base=USD'
    ];

    public function getRatesWithUsdBase()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $this->endpoints['allRatesBaseUsd']);

        return $response;
    }
}